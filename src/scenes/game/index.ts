import { GameObjects, Scene } from "phaser";
import { Player } from "../../classes/player";

export class GameScene extends Scene {
	private graves!: GameObjects.Sprite;
	private player!: Player;

	constructor() {
		super("game-scene");
	}
	create(): void {
		// this.graves = this.add.sprite(100, 100, "graves");
		// this.graves.setScale(1.5);
		this.player = new Player(this, 100, 100);
	}
	preload(): void {
		this.load.baseURL = "assets/";
		this.load.image("graves", "sprites/graves.png");
	}
	update(): void {
		this.player.update();
	}
}
