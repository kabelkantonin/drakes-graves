import { GameObjects, Scene } from "phaser";
export class LoadingScene extends Scene {
	private graves!: GameObjects.Sprite;
	constructor() {
		super("loading-scene");
	}
	create(): void {
		this.graves = this.add.sprite(100, 100, "graves");
		this.graves.setScale(1.5);
	}
	preload(): void {
		this.load.baseURL = "assets/";
		// key: 'king'
		// path from baseURL to file: 'sprites/king.png'
		this.load.image("graves", "sprites/graves.png");
	}
}
